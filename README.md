# 基本模板（Django + Mysql + Apache + Docker）

## 如何使用
1. 取得專案：`https://gitlab.com/as124122323/template-of-basic-django-with-apache-in-docker.git`
2. 將你的`Django Project`放入`www`目錄下
3. 替換樣板中的變量
    - 將`{DJANGO_PROJECT_NAME}`替換成你的`Django Project Name`（範例：`webapi`）
        ![DJANGO_PROJECT_NAME](https://gitlab.com/as124122323/template-of-basic-django-with-apache-in-docker/-/raw/main/readme_img/django-project-name.png?inline=false)
    - 將`{DJANGO_STATIC_ROOT}`替換成你的`Django Static Root`路徑（範例：`/var/www/static_root`）
        ![DJANGO_STATIC_ROOT](https://gitlab.com/as124122323/template-of-basic-django-with-apache-in-docker/-/raw/main/readme_img/django-static-root.png?inline=false)
    - 替換掉`Django` `settings.py`中的資料庫設定，如下：
        - `HOST`會根據你的專案名改變
        - 若要修改其他設定（`PASSWORD`之類的），須同步修改`docker-compose`裡`db`區段的設定
        ```python
        DATABASES = {
            'default': {
                'ENGINE': 'django.db.backends.mysql',  
                'NAME': 'webdb', 
                'USER': 'test',  
                'PASSWORD': 'test+123',
                'HOST': 'Django專案名_db',  # EX: webapi_db
                'PORT': '3306', 
            }
        }
        ```

## 佈署方式（develop）：
1. 進入專案目錄：`cd template-of-basic-django-with-apache-in-docker`
2. 建立`Image`：`bash docker/build_image.sh`
3. 啟動`Docker`服務：
	- 啟動指令：`bash docker.develop/deploy_stack.develop.sh`
	- 啟動服務包含：
		- Apache + Django
        - Mysql
	- 若要關閉`Docker`服務則運行：`bash docker.develop/remove_stack.develop.sh`

## 測試網址（develop）：
- domain：你的機器IP 或 127.0.0.1
- port：8888


