#!/usr/bin/bash

# 將Django的靜態檔蒐集至static_root(settings.py配置)中
source /opt/web_virtualenv/bin/activate
mkdir -p {DJANGO_STATIC_ROOT}
chown -R www-data:www-data {DJANGO_STATIC_ROOT}
python /var/www/{DJANGO_PROJECT_NAME}/manage.py collectstatic

# 將apache推到前台，這樣才能保持apache運行狀態
/usr/sbin/apachectl -D FOREGROUND